package com.ks.pruebas;

/**
 * Created by Miguel on 25/10/2016.
 */
public class Respuesta
{
    private String Componente;
    private String Configuracion;
    private String Estado;

    public String getComponente()
    {
        return Componente;
    }

    public void setComponente(String componente)
    {
        Componente = componente;
    }

    public String getConfiguracion()
    {
        return Configuracion;
    }

    public void setConfiguracion(String configuracion)
    {
        Configuracion = configuracion;
    }

    public String getEstado()
    {
        return Estado;
    }

    public void setEstado(String estado)
    {
        Estado = estado;
    }

}
